const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8000;

const password = 'rtuiechsjrk';

app.get('/get/encode/:name', (req, res) => {
    const codedWord = Vigenere.Cipher(password).crypt(req.params.name);
    res.send(codedWord);
});

app.get('/get/decode/:name', (req, res) => {
    const decodedWord = Vigenere.Decipher(password).crypt(req.params.name);
    res.send(decodedWord);
});

app.get('/get/:name', (req, res) => {
    res.send(req.params.name);
});

app.listen(port, () => {
    console.log('We are live on: ' + port);
});